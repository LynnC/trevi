package com.lynn.trevidemo

import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

/**
 * Created by lynnchen on 2018/10/28.
 */
class ResultPresenter(private val model: ResultModel,
                      private val view: ResultView) {

    companion object {
        private const val INTERVAL = 10L
    }

    private lateinit var disposable: Disposable

    fun onCreate() {
        view.waitForLayout { init() }
    }

    fun onConfigurationChanged() {
        view.waitForLayout { updateFrame() }
    }

    private fun init() {
        view.initGridLayout()
        startTimer(INTERVAL)
    }


    private fun startTimer(interval: Long) {
        val observable = Observable.interval(0, interval, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
        disposable = observable.subscribe {
            update()
        }
    }

    fun dispose() {
        disposable?.dispose()
    }

    private fun update() {
        clear()

        model.random()

        view.updateGridItem(model.getItemIndex(), true)
        view.updateBottomButton(model.getBottomButtonIndex(), true)

        view.setFrameVisibility(View.VISIBLE)
        updateFrame()
    }

    fun clear() {
        view.updateGridItem(model.getItemIndex(), false)
        view.updateBottomButton(model.getBottomButtonIndex(), false)
        view.setFrameVisibility(View.INVISIBLE)
    }

    private fun updateFrame() {
        view.updateFrame(model.getColumn())
    }
}