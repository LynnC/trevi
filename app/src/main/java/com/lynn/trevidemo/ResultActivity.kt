package com.lynn.trevidemo

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.GridLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity(), ResultView {

    private val TAG = "lynn::ResultActivity"

    companion object {
        private const val ARG_ROW_COUNT = "ROW_COUNT"
        private const val ARG_COL_COUNT = "COLUMN_COUNT"

        fun newIntent(context: Context, rowCount: Int, colCount: Int): Intent? {
            if (rowCount < 1 || colCount < 1) {
                return null
            }
            val intent = Intent(context, ResultActivity::class.java)
            intent.putExtra(ARG_ROW_COUNT, rowCount)
            intent.putExtra(ARG_COL_COUNT, colCount)
            return intent
        }
    }

    private var rowCount: Int = 0
    private var colCount: Int = 0

    private lateinit var presenter: ResultPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rowCount = intent.getIntExtra(ARG_ROW_COUNT, 0)
        colCount = intent.getIntExtra(ARG_COL_COUNT, 0)
        if (rowCount < 1 || colCount < 1) {
            finish()
            return
        }

        setContentView(R.layout.activity_result)
        select_frame.visibility = View.INVISIBLE

        val model = ResultModel(rowCount, colCount)
        presenter = ResultPresenter(model, this)
        presenter.onCreate()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        presenter.onConfigurationChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
    }

    override fun waitForLayout(f: () -> Unit) {
        layout.waitForLayout {
            f()
        }
    }

    override fun initGridLayout() {
        val inflater = LayoutInflater.from(applicationContext)
        val marginVertical = resources.getDimension(R.dimen.text_margin_vertical).toInt()
        val marginHorizontal = resources.getDimension(R.dimen.text_margin_horizontal).toInt()

        grid_layout.removeAllViews()
        grid_layout.rowCount = rowCount + 1
        grid_layout.columnCount = colCount

        for (row in 0 until rowCount) {
            val color = getRandomColor()
            for (col in 0 until colCount) {
                val childView = inflater.inflate(R.layout.layout_grid_item, null, false)
                childView.setBackgroundColor(color)

                val params = getLayoutParams(GridLayout.UNDEFINED, GridLayout.UNDEFINED)
                params.setMargins(marginVertical, marginHorizontal, marginVertical, 0)

                grid_layout.addView(childView, params)
            }
        }

        for (col in 0 until colCount) {
            val buttonBottom = inflater.inflate(R.layout.button_bottom, null, false)
            buttonBottom.setOnClickListener(buttonBottomOnClickListener)

            val params = getLayoutParams(rowCount, col)
            params.setMargins(0, marginHorizontal, 0, 0)

            grid_layout.addView(buttonBottom, params)
        }
    }

    private val buttonBottomOnClickListener = View.OnClickListener { presenter.clear() }

    override fun updateGridItem(index: Int, selected: Boolean) {
        Log.d(TAG, "[updateGridItem] index: " + index)
        val frameLayout = grid_layout.getChildAt(index) as FrameLayout? ?: return
        val textView = frameLayout.findViewById<TextView>(R.id.tv)
        textView.text = if (selected) getString(R.string.random) else ""
    }

    override fun updateBottomButton(index: Int, selected: Boolean) {
        val button: Button = grid_layout.getChildAt(index) as Button? ?: return

        button.setBackgroundResource(
                if (selected) R.drawable.button_frame_selected else R.drawable.button_frame_normal
        )
        button.setTextColor(getColorByVersion(
                if (selected) R.color.text_button_selected else R.color.text_button_normal))
    }

    override fun updateFrame(col: Int) {
        val activityWidth = window.decorView.width
        val width = activityWidth / colCount
        val left = width.toFloat() * col

        val layoutParams = select_frame.layoutParams
        layoutParams.width = width

        select_frame.layoutParams = layoutParams
        select_frame.x = left
    }

    override fun setFrameVisibility(visibility: Int) {
        select_frame.visibility = visibility
    }

    private inline fun View.waitForLayout(crossinline f: () -> Unit) = with(viewTreeObserver) {
        addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
                removeOnLayoutChangeListener(this)
                f()
            }
        })
    }

    private inline fun getLayoutParams(rowStart: Int, colStart: Int): GridLayout.LayoutParams {
        val rowSpec = GridLayout.spec(rowStart, GridLayout.FILL, 1f)
        val columnSpec = GridLayout.spec(colStart, GridLayout.FILL, 1f)

        val params = GridLayout.LayoutParams(rowSpec, columnSpec)
        params.height = 0
        params.width = 0
        params.setGravity(Gravity.FILL)

        return params
    }

    private inline fun getRandomColor() = Color.rgb(getRandomNumberOfColorComponent(), getRandomNumberOfColorComponent(), getRandomNumberOfColorComponent())

    private inline fun getRandomNumberOfColorComponent() = (0..255).shuffled().first()

    private inline fun getColorByVersion(colorRes: Int) =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                resources.getColor(colorRes, null)
            else
                resources.getColor(colorRes)
}
