package com.lynn.trevidemo

/**
 * Created by lynnchen on 2018/10/28.
 */
interface ResultView {

    fun waitForLayout(f: () -> Unit)
    fun initGridLayout()
    fun updateGridItem(index: Int, selected: Boolean)
    fun updateBottomButton(index: Int, selected: Boolean)
    fun updateFrame(col: Int)
    fun setFrameVisibility(visibility: Int)
}