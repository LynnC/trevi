package com.lynn.trevidemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun next(view: View) {
        if (TextUtils.isEmpty(et_col.text) || TextUtils.isEmpty(et_row.text)) {
            Toast.makeText(applicationContext, R.string.toast_fill_fields, Toast.LENGTH_SHORT).show()
            return
        }
        val col: Int? = try {
            et_col.text?.toString()?.toInt()
        } catch (e: NumberFormatException) {
            Toast.makeText(applicationContext, R.string.toast_fill_fields_with_int, Toast.LENGTH_SHORT).show()
            return
        }
        val row: Int? = try {
            et_row.text?.toString()?.toInt()
        } catch (e: NumberFormatException) {
            Toast.makeText(applicationContext, R.string.toast_fill_fields_with_int, Toast.LENGTH_SHORT).show()
            return
        }
        if (col!! < 1 || row!! < 1) {
            Toast.makeText(applicationContext, R.string.toast_fill_fields_with_int, Toast.LENGTH_SHORT).show()
            return
        }

        goNext(col, row)
    }

    private fun goNext(col: Int, row: Int) {
        val intent = ResultActivity.newIntent(applicationContext, row, col)
        if (intent == null) {
            Toast.makeText(applicationContext, R.string.toast_fill_fields_with_int, Toast.LENGTH_SHORT).show()
            return
        }
        startActivity(intent)
    }
}
