package com.lynn.trevidemo

import android.util.Log

/**
 * Created by lynnchen on 2018/10/28.
 */
class ResultModel(private val rowCount: Int,
                  private val colCount: Int) {

    private val TAG = "lynn::ResultPresenter"

    private var index = 0

    private val count: Int = rowCount * colCount

    fun random() {
        index = (0 until count).shuffled().first()

        Log.d(TAG, "[random] index = " + index)
    }

    fun getColumn(): Int {
        return index - index / colCount * colCount
    }

    fun getItemIndex(): Int {
        return index
    }

    fun getBottomButtonIndex(): Int {
        return rowCount * colCount + getColumn()
    }
}